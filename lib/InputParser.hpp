#include <vector>
#include <iostream>
#include <string>
#include <sstream>

std::vector<std::string> split(std::string const &s, char const &delim);

std::vector<std::string> split(std::string const &s, char const &delim){
    std::vector<std::string> lines;
    std::string line;
    std::istringstream data_stream(s);
    while(std::getline(data_stream, line, delim)){
        lines.push_back(line);
    }
    return lines;
}

