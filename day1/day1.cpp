#include <vector>
#include <string>
#include <iostream>
#include <cstdlib>


#define LIST1 "1122"
#define LIST2 "1111"
#define LIST3 "1234"
#define LIST4 "91212129"
#define TEST_HALFWAY1 "1212"
#define TEST_HALFWAY2 "1221"
#define TEST_HALFWAY3 "123425"
#define TEST_HALFWAY4 "123123"
#define TEST_HALFWAY5 "12131415"

void string_to_int_vector(std::string &input, std::vector<int> &output){
    output.clear();
    for(std::string::iterator it = input.begin(); it != input.end(); ++it){
        output.push_back(char(*it)-48);
    }
    output.push_back(*(input.begin())-48);
    return;
}

long sum_in_vector(std::vector<int> &input){
    long result = 0;

    std::cout << "Given list : ----------------------------" << std::endl;
    for(std::vector<int>::iterator it = input.begin(); it != input.end();++it){
        std::cout << *it;
    }
    std::cout << "\n" << std::endl;

    for(std::vector<int>::iterator it = input.begin(); it != input.end();
         ++it){
        //cpt = 0;
        while (it+1 != input.end() && *(it+1) == *it){
            std::cout << "it: " << *it << " and it+1: " << *(it+1) << std::endl;
            //cpt++;
            it++;
            result += *it;
        }
    }
    return result;
}


long halfway_around_sum(std::vector<int> &input){
    size_t half_size = (input.size()-1)/2;
    long result = 0;

    std::cout << "Given list : ===========================" << std::endl;
    std::cout << "First part: ----------------------------" << std::endl;
    for(std::vector<int>::iterator it = input.begin();
            it != input.begin()+half_size; ++it){
        std::cout << *it;
    }
    std::cout << "\nSecond part: ----------------------------" << std::endl;
    for(std::vector<int>::iterator it = input.begin()+half_size;
            it != input.end()-1; ++it){
        std::cout << *it;
    }
     std::cout << "\n" << std::endl;

    for(std::vector<int>::iterator it = input.begin();
            it != input.begin()+half_size; ++it){
        if(*it == *(it+half_size)){
            result += (*it)*2;
        }
    }
    return result;
}


int main (){
    std::cout << "DAY 1 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"<<std::endl;
    std::vector<int> numbers;
    std::string test;

    test = LIST1 ;
    string_to_int_vector(test, numbers);
    std::cout << "\nSum is : " << sum_in_vector(numbers) << std::endl;

    test = LIST2 ;
    string_to_int_vector(test, numbers);
    std::cout << "\nSum is : " << sum_in_vector(numbers) << std::endl;

    test = LIST3 ;
    string_to_int_vector(test, numbers);
    std::cout << "\nSum is : " << sum_in_vector(numbers) << std::endl;

    test = LIST4 ;
    string_to_int_vector(test, numbers);
    std::cout << "\nSum is : " << sum_in_vector(numbers) << std::endl;

    test = TEST_HALFWAY1;
    string_to_int_vector(test, numbers);
    std::cout << "Halway sum is : " << halfway_around_sum(numbers) << std::endl;

    test = TEST_HALFWAY2;
    string_to_int_vector(test, numbers);
    std::cout << "Halway sum is : " << halfway_around_sum(numbers) << std::endl;

    test = TEST_HALFWAY3;
    string_to_int_vector(test, numbers);
    std::cout << "Halway sum is : " << halfway_around_sum(numbers) << std::endl;

    test = TEST_HALFWAY4;
    string_to_int_vector(test, numbers);
    std::cout << "Halway sum is : " << halfway_around_sum(numbers) << std::endl;

    test = TEST_HALFWAY5;
    string_to_int_vector(test, numbers);
    std::cout << "Halway sum is : " << halfway_around_sum(numbers) << std::endl;

    std::cout << "\nInsert your list of numbers:\n" <<std::endl;
    getline(std::cin, test);
    string_to_int_vector(test, numbers);
    std::cout << "\nSum is : " << sum_in_vector(numbers) << std::endl;
    std::cout << "Halway sum is : " << halfway_around_sum(numbers) << std::endl;

    return 0;
}
