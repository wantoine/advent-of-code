#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <algorithm>
#include "InputParser.hpp"

#define TEST "5\t1\t9\t5\n7\t5\t3\n2\t4\t6\t8"

std::vector< std::vector<int> > parse_input(std::string const &input){
    std::vector<std::string> lines = split(input, '\n');

    std::vector< std::vector<int> > output;
    std::vector<int> numbers;
    std::vector<std::string> str_numbers;
    for(std::vector<std::string>::iterator line = lines.begin();
            line != lines.end(); ++line){
        str_numbers = split(*line, '\t');
        numbers.clear();
        for(std::vector<std::string>::iterator num = str_numbers.begin();
                num != str_numbers.end(); ++num){
            numbers.push_back(std::stoi(*num));
        }
        output.push_back(numbers);
    }

    for(std::vector< std::vector<int> >::iterator l = output.begin();
        l != output.end(); ++l){
        std::cout << "=";
        for(std::vector<int>::iterator c = l->begin(); c !=l->end(); ++c)
            std::cout << *c << "|";
        std::cout << "=" << std::endl;
    }

    return output;
}


class ChecksumComputer{
public:
    ChecksumComputer(){}
    ~ChecksumComputer(){}
    long compute_checksum(std::string const &input);
    long part_two(std::string const &input);
    long get_evenly_divisible_value(std::vector<int> const &numbers);
};

long ChecksumComputer::compute_checksum(std::string const &input){
    long output = 0;
    std::vector< std::vector<int> > numbers = parse_input(input);
    for(std::vector< std::vector<int> >::iterator line = numbers.begin();
            line != numbers.end(); ++line){
        output += *std::max_element(line->begin(), line->end())
                    - *std::min_element(line->begin(), line->end());
    }
    std::cout << "CHECKSUM:\n" << output << std::endl;
    return output;
}


long ChecksumComputer::get_evenly_divisible_value(
        std::vector<int> const &numbers){
    for(std::vector<int>::const_iterator n = numbers.begin();
            n != numbers.end(); ++n){
        for(std::vector<int>::const_iterator m = numbers.begin();
            m != numbers.end(); ++m){
            if(n!=m && (*m) % (*n) == 0)
                return (*m)/(*n);
        }
    }
    throw std::exception();
}


long ChecksumComputer::part_two(std::string const &input){
    long output = 0;
    std::vector< std::vector<int> > numbers = parse_input(input);
    for(std::vector< std::vector<int> >::iterator line = numbers.begin();
            line != numbers.end(); ++line){
        output += get_evenly_divisible_value(*line);
    }
    std::cout << "CHECKSUM PART 2:\n" << output << std::endl;
    return output;
}

int main(){
    ChecksumComputer csc;
    std::string test_string = TEST;
    std::vector< std::vector<int> > speadsheet;

    parse_input(TEST);
    csc.compute_checksum(TEST);
    std::string line;
    std::string data;
    std::cout << "\nInsert your list of numbers "
              << "(add en empty line at the end):\n"
              << std::endl;

    while(std::getline(std::cin, line)){
        if(*line.begin() == '\0'){
            break;
        }
        data.append(line + '\n');
    }
    std::cout << "READ : ~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n" << data <<std::endl;
    csc.compute_checksum(data);
    csc.part_two(data);
    return 0;
}
