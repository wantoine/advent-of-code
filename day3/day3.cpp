#include <iostream>
#include <cmath>
#include <vector>

class Position2D{
public:
    long x_;
    long y_;
    Position2D(): x_(0), y_(0){}
    Position2D(long const &x, long const &y): x_(x), y_(y){}
    ~Position2D(){}

    friend std::ostream &operator<<(std::ostream&, Position2D const&);
};


std::ostream &operator<<(std::ostream &os, Position2D const &pos){
    os << "Position2D: x = " << pos.x_ << "; y =" << pos.y_;
    return os;
}


long manhattan_dist(Position2D const &p1, Position2D const &p2){
    return (std::abs(p1.x_-p2.x_) + std::abs(p1.y_-p2.y_));
}


class Day3Solver{
private:
    void compute_target_pos();

protected:
    long target_val_;
    Position2D target_pos_;

public:
    Day3Solver(long const &input);
    ~Day3Solver(){}

    void answer_part1();
    void answer_part2();

};


Day3Solver::Day3Solver(long const &input):
        target_val_(input){
    compute_target_pos();
}


void Day3Solver::compute_target_pos(){
    if(target_val_==1){
        return;
    }
    long n = 1;
    while(n*n < target_val_)
        n+=2;
    long width = n/2 ;
    //std::cout << "Size of the grid: " << width
    //          << " n = " << n <<std::endl;
    long diff = n*n - target_val_;
    long quotient = diff/(n-1);
    long rest = diff%(n-1);

    //std::cout << "Quotient: " << quotient << "; rest: " << rest << std::endl;
    if(quotient == 3){
        // x>0, move along y from (width, width)
        target_pos_.x_ = width;
        target_pos_.y_ = width - rest;
    }
    else if(quotient == 2){
        // y>0, move along x from (-width, width)
        target_pos_.x_ = -width + rest;
        target_pos_.y_ = width;
    }
    else if(quotient == 1){
        // x<0, move along y from (-width, -width)
        target_pos_.x_ = -width;
        target_pos_.y_ = -width + rest;
    }
    else if(quotient == 0){
        // y<0, move along x from (width, -width)
        target_pos_.x_ = width - rest;
        target_pos_.y_ = -width;
    }
    std::cout << "Position of target in grid: " << target_pos_ << std::endl;

}


void Day3Solver::answer_part1(){
    std::cout << "\t===> Manhattan distance to 1 is : "
              << manhattan_dist(target_pos_, Position2D(0, 0))
              <<std::endl;
}


void foo(){
    std::vector< std::vector<int> > v;
    size_t n = 0;

    for(size_t i:)

    for(std::vector<int>::iterator it=v.begin(); it!=v.end(); ++it){
        std::cout<<*it<< " ";
    }
    std::cout<<std::endl;

    std


}


int main(){
    // tests:
    long values[] = {1, 2, 12, 23, 1024, 9, 16, 25, 49};
    std::vector<long> test_values(values, values + sizeof(values)/sizeof(long));
    for(std::vector<long>::iterator n = test_values.begin();
        n != test_values.end(); ++n){
        std::cout << "\nTest with : " << *n << " ------------------------"
                  <<std::endl;
        Day3Solver s(*n);
        s.answer_part1();
    }
    foo();
    /*

    std::cout << "\nInsert your target:\n" <<std::endl;
    std::string data;
    getline(std::cin, data);
    Day3Solver solve(std::stol(data));
    solve.answer_part1();
    */
    return 0;
}
